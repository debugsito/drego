<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class ContactoController extends Controller
{
    public function index(Request $request)
    {
        $data = $request->all();
        Mail::send('mails.cliente', $data, function ($messase) use ($data) {
            $messase->to(env('EMAIL_ADMIN','csramosflores@gmail.com'));
            $messase->subject('Cliente desde la web DR. EGO');
        });
        Session::flash('flash_message', 'Gracias por escribir, nos contactaremos a la brevedad.');
        return redirect('/contacto');
    }
}
