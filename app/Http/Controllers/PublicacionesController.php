<?php

namespace App\Http\Controllers;

use App\Publicacione;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PublicacionesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $publicaciones = Publicacione::where('title', 'LIKE', "%$keyword%")
                ->orWhere('url', 'LIKE', "%$keyword%")->orderBy('id','desc')
                ->paginate($perPage);
        } else {
            $publicaciones = Publicacione::orderBy('id','desc')->paginate($perPage);
        }

        return view('publicaciones.index', compact('publicaciones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('publicaciones.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'imagen' => 'required',
            'url' => 'required'
        ]);
        $path = Storage::disk('uploads')->put('images', $request->imagen);
        $requestData = $request->all();
        $requestData['imagen'] = $path;

        Publicacione::create($requestData);

        return redirect('admin/publicaciones')->with('flash_message', 'Publicacione added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $publicacion = Publicacione::findOrFail($id);

        return view('publicaciones.show', compact('publicacion'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $publicacion = Publicacione::findOrFail($id);

        return view('publicaciones.edit', compact('publicacion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'imagen' => 'required',
            'url' => 'required'
        ]);
        $path = Storage::disk('uploads')->put('images', $request->imagen);
        $requestData = $request->all();
        $requestData['imagen'] = $path;

        $publicacion = Publicacione::findOrFail($id);
        $publicacion->update($requestData);

        return redirect('admin/publicaciones')->with('flash_message', 'Publicacione updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Publicacione::destroy($id);

        return redirect('admin/publicaciones')->with('flash_message', 'Publicacione deleted!');
    }

}
