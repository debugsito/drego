<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Publicacione extends Model
{
    protected $fillable = ['title', 'imagen', 'url'];
}
