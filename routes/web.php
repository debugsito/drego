<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/dr-ego', function () {
    return view('dr-ego');
});
Route::get('/contacto', function () {
    return view('contacto');
});


Route::get('/consultorio', function () {
    return view('consultorio');
});
Route::get('/domicilio', function () {
    return view('domicilio');
});
Route::get('/sala-operaciones', function () {
    return view('sala-operaciones');
});
Route::get('/videos', function () {
    $videos = \App\Video::orderBy('id','desc')->get();
    return view('videos', compact('videos'));
});
Route::get('/publicaciones', function () {
    $publicaciones = \App\Publicacione::orderBy('id','desc')->get();
    return view('publicaciones',compact('publicaciones'));
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('admin/videos', 'VideosController')->middleware('auth');;
Route::resource('admin/publicaciones', 'PublicacionesController')->middleware('auth');;

Route::post('contacto', ['as' => 'contacto', 'uses' => 'ContactoController@index']);
