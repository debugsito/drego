<div class="col-md-3">
    {{--<div class="panel panel-default panel-flush">
        <div class="panel-heading">
            Sidebar
        </div>

        <div class="panel-body">
            <ul class="nav" role="tablist">
                <li role="presentation">
                    <a href="{{ url('/admin') }}">
                        Dashboard
                    </a>
                </li>
            </ul>
        </div>
    </div>--}}
    <div class="panel panel-default panel-flush">
        <div class="panel-heading">
            Mantenimiento
        </div>

        <div class="panel-body">
            <ul class="nav" role="tablist">
                <li role="presentation">
                    <a href="{{ url('/admin/videos') }}">
                        Videoteca
                    </a>
                </li>
                <li role="presentation">
                    <a href="{{ url('/admin/publicaciones') }}">
                        Publicaciones
                    </a>
                </li>
            </ul>
        </div>
    </div>

</div>
