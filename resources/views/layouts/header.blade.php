<header <?php echo $_SERVER['REQUEST_URI']=='/'?'':'class="headsombra"' ?>>
    <div class="contenedor">
        <a href="{{ url('/') }}" class="logo"></a>

        <p class="botonmenu"></p>
        <nav class="menublock">

            <ul>
                <li><a href="{{ url('/dr-ego') }}" >DR.EGO </a></li>
                <li class="curso">
                    <a href="#" class="submenu1">CENTRO DE INFORMACIÓN</a>
                    <ul class="boxsub1">
                        <li><a href="{{ url('/videos') }}">VIDEOTECA</a></li>
                        <li><a href="{{ url('/publicaciones') }}">PUBLICACIONES</a></li>

                    </ul>
                </li>

                <li class="space">
                    <p></p>
                </li>
                <li class="curso">
                    <a href="#" class="submenu2">SERVICIOS</a>
                    <ul class="boxsub2">
                        <li><a href="{{ url('/consultorio') }}">CONSULTORIO</a></li>
                        <li><a href="{{ url('/domicilio') }}">ATENCIÓN A DOMICILIO</a></li>
                        <li><a href="{{ url('/sala-operaciones') }}">SALA DE OPERACIONES</a></li>

                    </ul>

                </li>
                <li >
                    <a href="{{ url('/contacto') }}" class="">CONTACTO</a>
                </li>


                <div class="clear"></div>
            </ul>



        </nav>
    </div>
</header>