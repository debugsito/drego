<nav class="container header-navbar is-fluid desktop-only">
    <div class="columns">
        <div class="column text-center">
            <ul class="navbar-right">
                <li><a href="{{ route('conocenos') }}">{{ __("Conocenos") }}</a></li>♦
                <li><a href="{{ route('catalogo') }}">{{ __("Patrones") }}</a></li>♦
                <li><a href="{{ route('ideas') }}">{{ __("Ideas y Aplicaciones") }}</a></li>♦
                <li><a href="{{route('contacto')}}">{{ __("Contacto") }}</a></li>♦
            </ul>
        </div>
        <div class="column text-center">
            <ul class="navbar-left">
                <li><a href="{{ route('cuenta') }}">{{ __("Cuenta") }}</a></li>♦
                <li><a href="{{ route('patrones') }}">{{ __("Mis Patrones") }}</a></li>♦
                @if (Auth::guest())
                    <li><a href="{{ route('login') }}">{{ __("Login") }}</a></li>♦
                    <li><a href="{{ route('register') }}">{{ __("Registro") }}</a></li>
                @else
                    <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
               document.getElementById('logout-form').submit();">{{ __("Logout") }}</a></li>
                @endif
                <li class="container-shopping-cart">
					        <a href="{{ route('carrito') }}"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <span id="cantidad_carrito"><?php echo count(session()->get('carrito')) ?></span></a>
				        </li>
            </ul>
        </div>
    </div>
</nav>
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    {{ csrf_field() }}
</form>
<!-- Navbar-Responsive -->
<div class="navbar-component responsive-only">
    <div class="navbar area">
        <a href="{{ url('/') }}" class="brand"><img src="{{ asset('img/logotipo.png') }}" alt="Vito-Loli-Patterns-"></a>
        <div class="container-social-media column">
            <div class="columns">
                <div class="idioma">
                    <a href="{{ route('set.locale',array('locale'=>'es')) }}"><span>español</span></a>
                    <a href="{{ route('set.locale',array('locale'=>'en')) }}"><span>english</span></a>
                    <a href="{{ route('set.locale',array('locale'=>'zh')) }}"><span>中文(香港)</span></a>
                </div>
            </div>
        </div>
        <nav role="navigation" id="navigation" class="list">
            <a href="{{ route('conocenos') }}" class="item -link">{{ __("Conocenos") }}</a>
            <a href="{{ route('catalogo') }}" class="item -link">{{ __("Patrones") }}</a>
            <a href="{{ route('ideas') }}" class="item -link">{{ __("Ideas y Aplicaciones") }}</a>
            <a href="{{route('contacto')}}" class="item -link">{{ __("Contacto") }}</a>
            <a href="{{ route('cuenta') }}" class="item -link">{{ __("Cuenta") }}</a>
            <a href="{{ route('patrones') }}" class="item -link">{{ __("Mis Patrones") }}</a>
            @if (Auth::guest())
                <a href="{{ route('login') }}" class="item -link">{{ __("Login") }}</a>
                <a href="{{ route('register') }}" class="item -link">{{ __("Registro") }}</a>
            @else
                <a href="{{ route('logout') }}" onclick="event.preventDefault();
               document.getElementById('logout-form').submit();" class="item -link">{{ __("Logout") }}</a>
            @endif
        </nav>
        <button data-collapse data-target="#navigation" class="toggle">
            <span class="icon"></span>
        </button>
    </div>
</div>
