<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{--<title>{{ config('app.name', 'Laravel') }}</title>--}}

    <!-- Styles -->

    <title>DR. EGO | Carlos Calderón Ginecología y Obstetricia</title>
    <link rel="shortcut icon" href="{{ asset('imgs/favicon.ico') }}">
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/estilos.css') }}" media="all">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/respo.css') }}" media="all">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>

    <meta property="og:title" content="DR. EGO | Carlos Calderón Ginecología y Obstetricia" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="http://www.drego.pe" />
    <meta property="og:image" content="http://www.drego.pe/compartir-face/1.jpg" />
    <meta property="og:description" content="Conoce al Dr. EGO" />

    <link href="{{ asset('css/main.css') }}" rel="stylesheet" media="screen"/>

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-92481568-1', 'auto');
        ga('send', 'pageview');

    </script>


    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="{{ asset('css/mosaic.css') }}" type="text/css" media="screen" />
    <script type="text/javascript" src="{{ asset('js/mosaic.1.0.1.js') }}"></script>
    <script type="text/javascript">
        jQuery(function($){
            $('.fade').mosaic();
        });
    </script>
</head>
<body>
@yield('content')
@include('layouts/footer')
</body>

<!--SCRIPTS  -->
@yield('scripts')

<script src='{{ asset('js/script2.js') }}'></script>

<!-- jQuery -->
<script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
<!-- Client JavaScript -->
<script src="{{ asset('js/jquery.reslider.js') }}"></script>

<script>
    $(function(){
        $('.jquery-reslider').reSlider({
            speed:1000,//设置轮播的高度
            delay:7000,//设置轮播的延迟时间
            imgCount:3,//设置轮播的图片数
            dots:true,//设置轮播的序号点
            autoPlay:true//设置轮播是否自动播放
        })
    })
</script>
</html>
