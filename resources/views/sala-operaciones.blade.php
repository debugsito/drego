@extends('layouts.appd')
<div class="fondoimg fondo5">
    @include('layouts/header')
</div>
@section('content')
    <div class="contenedor">




        <div class="boxconsult">

            <img src="imgs/histerectomia.jpg" class="imgconsulta">
            <div class="infoconsult leftserv">
                <h2 class="titulo">Histerectomía</h2>
                <p class="texto-gray">Es un procedimiento quirúrgico por el cual se extrae el útero o matriz, se puede realizar por técnica vaginal, abdominal y/o laparoscópica; la técnica tiene distintas variantes, en algunos casos se puede extraer de manera simultánea el útero y los ovarios (uno o ambos) y/o decidir coservarlos. Cada procedimiento tiene sus razones y si necesitas que te las explique, cuenta conmigo; Dr. Ego. </p>

                <a href="contacto.php" class="solicitar">Escríbeme</a>

                <a href="http://www.facebook.com/sharer.php?s=100&amp;p[url]=http://www.drego.pe/sala-de-operaciones.php&amp;p[title]=DR. EGO | Carlos Calderón Ginecología y Obstetricia&amp;p[summary]=DR. EGO | Carlos Calderón Ginecología y Obstetricia&amp;p[images][0]=http://www.drego.pe/compartir-face/1.jpg" target="_blank" class="faceinfo"><img src="imgs/face.png"></a>

                <div class="clear"></div>


            </div>

            <div class="clear"></div>
        </div>


        <div class="boxconsult">
            <div class="infoconsult">
                <h2 class="titulo">Colposacropexia </h2>
                <p class="texto-gray">Es una técnica quirúrgica abierta o laparoscópica realizada en pacientes que ya no tienen útero por un antecedente quirúrgico previo para revertir el prolapso de la cúpula vaginal o muñón vaginal. En este procedimiento se coloca una malla por vía abdominal, la cual se sutura al periostio sacro para evitar que se vuelva a prolapsar. Cada indicación tiene sus razones y si necesitas que te las explique, cuenta conmigo; Dr. Ego.
                </p>

                <a href="contacto.php" class="solicitar">Escríbeme</a>
                <a href="http://www.facebook.com/sharer.php?s=100&amp;p[url]=http://www.drego.pe/sala-de-operaciones.php&amp;p[title]=DR. EGO | Carlos Calderón Ginecología y Obstetricia&amp;p[summary]=DR. EGO | Carlos Calderón Ginecología y Obstetricia&amp;p[images][0]=http://www.drego.pe/compartir-face/1.jpg" target="_blank" class="faceinfo"><img src="imgs/face.png"></a>
                <div class="clear"></div>
            </div>
            <img src="imgs/colpo.jpg" class="imgconsulta margright">
            <div class="clear"></div>
        </div>


        <div class="boxconsult">
            <img src="imgs/miomectomia.jpg" class="imgconsulta">
            <div class="infoconsult leftserv">
                <h2 class="titulo">Miomectomía</h2>
                <p class="texto-gray"> Es un procedimiento quirúrgico conservador por el cual, se retira exclusivamente el o los miomas uterinos, se puede realizar por técnica abierta y/o laparoscópica, teniendo en cuenta su tamaño y localización, buscando no dañar el útero. Se efectúa principalmente con fines reproductivos. Cada indicación tiene sus razones y si necesitas que te las explique, cuenta conmigo; Dr. Ego.
                </p>
                <a href="contacto.php" class="solicitar">Escríbeme</a>
                <a href="http://www.facebook.com/sharer.php?s=100&amp;p[url]=http://www.drego.pe/sala-de-operaciones.php&amp;p[title]=DR. EGO | Carlos Calderón Ginecología y Obstetricia&amp;p[summary]=DR. EGO | Carlos Calderón Ginecología y Obstetricia&amp;p[images][0]=http://www.drego.pe/compartir-face/1.jpg" target="_blank" class="faceinfo"><img src="imgs/face.png"></a>
                <div class="clear"></div>
            </div>

            <div class="clear"></div>
        </div>



        <div class="boxconsult">
            <div class="infoconsult">
                <h2 class="titulo">Bloqueo Tubárico Bilateral </h2>
                <p class="texto-gray">Es un procedimiento de planificación familiar definitivo, que tiene como objetivo extirpar un segmento de las trompas uterinas para evitar la unión del óvulo con los espermatozoides. Se puede realizar por vía laparoscópica o por vía abierta postparto (mediante una pequeña incisión en el ombligo). Cada indicación tiene sus razones y si necesitas que te las explique, cuenta conmigo; Dr. Ego.
                </p>
                <a href="contacto.php" class="solicitar">Escríbeme</a>
                <a href="http://www.facebook.com/sharer.php?s=100&amp;p[url]=http://www.drego.pe/sala-de-operaciones.php&amp;p[title]=DR. EGO | Carlos Calderón Ginecología y Obstetricia&amp;p[summary]=DR. EGO | Carlos Calderón Ginecología y Obstetricia&amp;p[images][0]=http://www.drego.pe/compartir-face/1.jpg" target="_blank" class="faceinfo"><img src="imgs/face.png"></a>
                <div class="clear"></div>
            </div>
            <img src="imgs/bloqueo.jpg" class="imgconsulta margright">
            <div class="clear"></div>
        </div>


        <div class="boxconsult">
            <img src="imgs/histeroscopia.jpg" class="imgconsulta">
            <div class="infoconsult leftserv">
                <h2 class="titulo"> Histeroscopía</h2>
                <p class="texto-gray"> Es un procedimiento mínimamente invasivo enfocado en el estudio de la cavidad uterina, es una prueba que se realiza por vía vaginal, y por medio de una cámara se observa el interior de la cavidad uterina con dos  objetivos: obtener un diagnóstico (encontrar causas o enfermedades) y/o de tratamiento (extirpar tumores endouterinos). Es una técnica muy útil en fertilidad y su recuperación postoperatoria es sumamente rápida. Cada procedimiento tiene sus razones y si necesitas que te las explique, cuenta conmigo; Dr. Ego.
                </p>
                <a href="contacto.php" class="solicitar">Escríbeme</a>
                <a href="http://www.facebook.com/sharer.php?s=100&amp;p[url]=http://www.drego.pe/sala-de-operaciones.php&amp;p[title]=DR. EGO | Carlos Calderón Ginecología y Obstetricia&amp;p[summary]=DR. EGO | Carlos Calderón Ginecología y Obstetricia&amp;p[images][0]=http://www.drego.pe/compartir-face/1.jpg" target="_blank" class="faceinfo"><img src="imgs/face.png"></a>
                <div class="clear"></div>
            </div>

            <div class="clear"></div>
        </div>



        <div class="boxconsult">
            <div class="infoconsult">
                <h2 class="titulo"> Quistectomía de ovario</h2>
                <p class="texto-gray">Es un procedimiento quirúrgico por el cual se realiza la extracción de la cápsula y el contenido de un tumor de ovario, sin importar si es líquido, solido o mixto. Se puede efectuar por técnica abierta y/o laparoscópica. Durante la técnica solo se extrae el tejido dañado sin afectar el tejido ovárico sano, dejando el ovario residual para su pronta recuperación. Cada indicación tiene sus razones y si necesitas que te las explique, cuenta conmigo; Dr. Ego.
                </p>
                <a href="contacto.php" class="solicitar">Escríbeme</a>
                <a href="http://www.facebook.com/sharer.php?s=100&amp;p[url]=http://www.drego.pe/sala-de-operaciones.php&amp;p[title]=DR. EGO | Carlos Calderón Ginecología y Obstetricia&amp;p[summary]=DR. EGO | Carlos Calderón Ginecología y Obstetricia&amp;p[images][0]=http://www.drego.pe/compartir-face/1.jpg" target="_blank" class="faceinfo"><img src="imgs/face.png"></a>
                <div class="clear"></div>
            </div>
            <img src="imgs/quiste.jpg" class="imgconsulta margright">
            <div class="clear"></div>
        </div>



        <div class="boxconsult">
            <img src="imgs/malla.jpg" class="imgconsulta">
            <div class="infoconsult leftserv">
                <h2 class="titulo"> Malla Suburetral TOT</h2>
                <p class="texto-gray">Es una técnica quirúrgica por vía vaginal, para tratar pacientes con incontinencia urinaria de esfuerzo (durante la tos o al cargar peso). Se coloca una malla en el tercio medio de la uretra para recuperar la competencia del esfínter uretral y disminuir el escape de orina. No se realiza en todos los casos de pacientes con incontinencia; cada indicación tiene sus razones y si necesitas que te las explique, cuenta conmigo; Dr. Ego.
                </p>
                <a href="contacto.php" class="solicitar">Escríbeme</a>
                <a href="http://www.facebook.com/sharer.php?s=100&amp;p[url]=http://www.drego.pe/sala-de-operaciones.php&amp;p[title]=DR. EGO | Carlos Calderón Ginecología y Obstetricia&amp;p[summary]=DR. EGO | Carlos Calderón Ginecología y Obstetricia&amp;p[images][0]=http://www.drego.pe/compartir-face/1.jpg" target="_blank" class="faceinfo"><img src="imgs/face.png"></a>
                <div class="clear"></div>
            </div>

            <div class="clear"></div>
        </div>




    </div>



    <div class="clear"></div>
@endsection