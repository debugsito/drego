@extends('layouts.appd')

@section('content')
    <div class="fondoimg fondo4">
        @include('layouts/header')
    </div>
    <?php foreach ($publicaciones as $publicacion){ ?>
    <div class="mosaic-block fade">
        <a href="{{ $publicacion->url }}" class="details mosaic-overlay" target="_blank">
            <h1>{{ $publicacion->title }}</h1>
        </a>

        <div class="mosaic-backdrop">
            <img src="{{ asset('uploads/'.$publicacion->imagen) }}">
        </div>
    </div>
    <?php } ?>
    <div class="clear"></div>
@endsection