@extends('layouts.appd')
<div class="fondoimg">
    @include('layouts/header')
</div>
@section('content')
<div class="contenedor">


    <ul class="botonesego">
        <li><a href="{{ url('/dr-ego') }}" class="activo">Dr Ego</a></li>
        <li><a href="medico.php">Médico</a></li>
        <li><a href="compromiso.php">Compromiso</a></li>

        <div class="clear"></div>
    </ul>



    <div class="imgboxego">
        <h2 class="titulo">Dr. EGO</h2>
        <p class="texto-gray textego">

            ¡Hola! ¿Cómo estás? Soy Carlos Calderón, soy Dr. Ego... A partir de ahora estaré cerca de ustedes para aclarar todas sus dudas sobre el ámbito de la ginecología y obstetricia. Considero que una paciente informada es una paciente con mayor capacidad para afrontar una enfermedad y conjuntamente con nosotros, los médicos, podremos curar o mejorar su salud en el menor tiempo posible. <br> <br>

            Trataré por medio de videos explicativos, artículos y con ayuda de la tecnología que comprendan que una enfermedad o un periodo de vida, no debe estar acompañado de miedos. <br> <br>
            Atte. Dr. Ego


        </p>

        <a href="contacto.php" class="solicitar">Escríbeme</a>
        <a href="" class="btnred"><img src="imgs/faceb.png"></a>
        <a href="" class="btnred"><img src="imgs/twit.png"></a>
        <a href="" class="btnred"><img src="imgs/yout.png"></a>
        <a href="" class="btnred"><img src="imgs/linke.png"></a>

        <div class="clear"></div>
    </div>
    <img src="imgs/DREGO.jpg" class="fotoego">


    <div class="clear"></div>




</div>



<div class="clear"></div>
    @endsection