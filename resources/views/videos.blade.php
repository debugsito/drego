@extends('layouts.appd')
<div class="fondoimg fondo3">
    @include('layouts/header')
</div>
@section('content')
    <div class="contenedor">

        <div class="boximages">

            <?php foreach ($videos as $video){ ?>
            <div class="videobox">

                <iframe width="560" height="315" class="video"  src="{{ str_replace('watch?v=','embed/',$video->url) }}" frameborder="0" allowfullscreen></iframe>


                <h2 class="titulo titvideo">{{ $video->title }}</h2>
                <p class="textovideo">{{ $video->descripcion }}</p>

                <!-- <a href="​​https://www.youtube.com/watch?v=RvYNEdQMuDo" target="_blank" >Ver Video</a> -->

                <a href="{{ $video->url }}" class="btnvervideo" target="_blank">Ver Video</a>
                <div class="boxvideored">

                    <a href="https://www.facebook.com/drego.pe/" target="_blank" class="faceinfo"><img src="imgs/face.png"></a>
                    <a href="https://www.youtube.com/channel/UCyGHIzvRTDDmNfUcjyvhDdQ" target="_blank" class="faceinfo"><img src="imgs/play.png"></a>
                    <a href="https://www.linkedin.com/in/carlos-mois%C3%A9s-calder%C3%B3n-pittaluga-a92958120" target="_blank" class="faceinfo"><img src="imgs/in.png"></a>
                    <a href="https://twitter.com/EgoDr" target="_blank" class="faceinfo"><img src="imgs/twi.png"></a>

                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>
            <?php } ?>

        </div>
        <div class="clear"></div>
    </div>
@endsection