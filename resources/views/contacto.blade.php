@extends('layouts.appd')
<div class="fondoimg fondo7">
    @include('layouts/header')
</div>
@section('content')
    <style>
        .alert {
            -webkit-border-radius: 0;
            -moz-border-radius: 0;
            -ms-border-radius: 0;
            border-radius: 0;
            -webkit-box-shadow: none;
            -moz-box-shadow: none;
            -ms-box-shadow: none;
            box-shadow: none;
            border: none;
            color: #fff !important;
        }
        .alert-success {
            color: #3c763d;
            background-color: #2b982b;
            border-color: #d6e9c6;
        }
        .alert {
            padding: 15px;
            margin-bottom: 20px;
            border: 1px solid transparent;
            border-radius: 4px;
        }
    </style>
    <div class="contenedor">
        @if(Session::has('flash_message'))
            <div class="alert alert-success">
                {{Session::get('flash_message')}}
            </div>
        @endif
        <div class="boxcontac">
            <div class="leftbox">

                <img src="imgs/contacto.png">
                <!-- <h2 class="titulos"> <strong>Contáctenos </strong><br>
                Rellena el siguiente formulario y me estaré comunicando contigo lo antes posible</h2> <br> -->

            <!--             <p class="texto">
                    <strong>Teléfonos</strong> <br>
                Cel. 951.253.493 / 946.440.850 <br>
                e-mail :
                drcalderon@drego.pe</p>
 -->


            </div>


            <form name="theform" id="theform" action="{{ url('/contacto') }}" method="POST">
                {{ csrf_field() }}
                <div class="caja">
                    <p class="rotulo">* Nombre</p>
                    <input name="nombre" type="text" class="cajatexto" id="nombre">
                </div>

                <div class="caja">
                    <p class="rotulo">* Email</p>
                    <input name="correo" type="mail" class="cajatexto" id="correo">
                </div>

                <div class="caja">
                    <p class="rotulo">* Teléfono</p>
                    <input name="telefono" type="text" class="cajatexto" id="telefono">
                </div>


                <div class="caja">
                    <p class="rotulo">* Mensaje</p>
                    <textarea class="mensaje cajatexto" name="comentario" id="comentario"></textarea>
                </div>

                <div class="bloque-boton">
                    <input class="boton-enviar" name="boton" type="submit" value="Enviar">
                </div>
            </form>

            <div class="clear"></div>
        </div>
    </div>

@endsection