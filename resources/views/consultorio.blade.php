@extends('layouts.appd')
<div class="fondoimg fondo55">
    @include('layouts/header')
</div>
@section('content')
    <div class="contenedor">




        <div class="boxconsult">

            <img src="imgs/papanicolao.jpg" class="imgconsulta">
            <div class="infoconsult leftserv">
                <h2 class="titulo">Prueba de Papanicolau</h2>
                <p class="texto-gray">Es una  prueba de despistaje para cáncer de cuello uterino, que consiste en la toma de un frotis del epitelio del cuello uterino, mediante la tinción del mismo nombre (papanicolau),  cuyo fin es identificar células que puedan estar afectadas. Se debe iniciar  2 años después de la primera relación sexual y realizar como mínimo 1 vez al año. Ante cualquier duda, no olvides consultar con tu ginecólogo Dr. Ego.</p>

                <a href="contacto.php" class="solicitar">Solicitar servicio</a>
                <!--       <a href="https://www.facebook.com/Dr-Ego-1001316999974599" target="_blank" class="faceinfo"><img src="imgs/face.png"></a> -->

                <!-- <iframe src="https://www.facebook.com/plugins/share_button.php?href=https%3A%2F%2Fwww.facebook.com%2Fdrego.pe%2F&layout=button&mobile_iframe=true&width=79&height=20&appId" width="79" height="20" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" class="faceinfo"></iframe>
                 -->

                <a href="http://www.facebook.com/sharer.php?s=100&amp;p[url]=http://www.drego.pe/consultorio.php&amp;p[title]=DR. EGO | Carlos Calderón Ginecología y Obstetricia&amp;p[summary]=DR. EGO | Carlos Calderón Ginecología y Obstetricia&amp;p[images][0]=http://www.drego.pe/compartir-face/1.jpg" target="_blank" class="faceinfo"><img src="imgs/face.png"></a>

                <div class="clear"></div>


            </div>

            <div class="clear"></div>
        </div>



        <div class="boxconsult">
            <div class="infoconsult">
                <h2 class="titulo">Colposcopia </h2>
                <p class="texto-gray">Es un examen de ayuda diagnóstica, mediante el cual con el uso de un lente de aumento y unas soluciones especiales, evaluamos posibles lesiones precancerosas tanto del cuello uterino, vagina y vulva. Recuerda que es un complemento al papanicolau e incrementa al doble las posibilidades de encontrar lesiones sospechosas. <br> Su importancia radica también en la evaluación de lesiones vulvares y vaginales asociadas a cáncer. Ante cualquier duda, no olvides consultar con tu ginecólogo Dr. Ego.
                </p>

                <a href="contacto.php" class="solicitar">Solicitar servicio</a>
                <a href="http://www.facebook.com/sharer.php?s=100&amp;p[url]=http://www.drego.pe/consultorio.php&amp;p[title]=DR. EGO | Carlos Calderón Ginecología y Obstetricia&amp;p[summary]=DR. EGO | Carlos Calderón Ginecología y Obstetricia&amp;p[images][0]=http://www.drego.pe/compartir-face/1.jpg" target="_blank" class="faceinfo"><img src="imgs/face.png"></a>



            </div>
            <img src="imgs/colo.jpg" class="imgconsulta margright">
            <div class="clear"></div>
        </div>


        <div class="boxconsult">
            <img src="imgs/crio.jpg" class="imgconsulta">
            <div class="infoconsult leftserv">
                <h2 class="titulo">Crioterapia</h2>
                <p class="texto-gray">Es una terapia por la cual destruimos tejido dañado del cuello uterino con la aplicación de nitrógeno líquido a temperaturas de hasta -196°C. Es un procedimiento ambulatorio, no duele y se puede hacer en consultorio en corto tiempo. Lo usamos para tratamiento en cervicitis graves y las lesiones pre malignas de bajo grado (NIC 1). Ante cualquier duda, no olvides consultar con tu ginecólogo Dr. Ego.
                </p>
                <a href="contacto.php" class="solicitar">Solicitar servicio</a>
                <a href="http://www.facebook.com/sharer.php?s=100&amp;p[url]=http://www.drego.pe/consultorio.php&amp;p[title]=DR. EGO | Carlos Calderón Ginecología y Obstetricia&amp;p[summary]=DR. EGO | Carlos Calderón Ginecología y Obstetricia&amp;p[images][0]=http://www.drego.pe/compartir-face/1.jpg" target="_blank" class="faceinfo"><img src="imgs/face.png"></a>

                <div class="clear"></div>
            </div>

            <div class="clear"></div>
        </div>



        <div class="boxconsult">
            <div class="infoconsult">
                <h2 class="titulo">Test de Cobas </h2>
                <p class="texto-gray">Es una prueba que detecta el ADN de 14 tipos de virus de papiloma humano (VPH) de alto riesgo, específicamente el 16, el 18 y otros 12 tipos, que pueden provocar cáncer de cuello uterino. El proceso es similar a la prueba de Papanicolaou y una vez tomada la muestra se envía para ser analizada. Esta prueba no solo te indica la presencia de VPH sino también brinda información a la paciente sobre un futuro riesgo de cáncer de cuello uterino. Ante cualquier duda, no olvides consultar con tu ginecólogo Dr. Ego.
                </p>
                <a href="contacto.php" class="solicitar">Solicitar servicio</a>
                <a href="http://www.facebook.com/sharer.php?s=100&amp;p[url]=http://www.drego.pe/consultorio.php&amp;p[title]=DR. EGO | Carlos Calderón Ginecología y Obstetricia&amp;p[summary]=DR. EGO | Carlos Calderón Ginecología y Obstetricia&amp;p[images][0]=http://www.drego.pe/compartir-face/1.jpg" target="_blank" class="faceinfo"><img src="imgs/face.png"></a>

                <div class="clear"></div>
            </div>
            <img src="imgs/test.jpg" class="imgconsulta margright">
            <div class="clear"></div>
        </div>


        <div class="boxconsult">
            <img src="imgs/eco.jpg" class="imgconsulta">
            <div class="infoconsult leftserv">
                <h2 class="titulo">Ecografía Transvaginal</h2>
                <p class="texto-gray">: Es un examen  de ayuda diagnóstica para evaluar útero y ovarios. Se coloca un transductor a través de la vagina y se observan las imágenes en pantalla, con los cuales su ginecólogo identificará si alguna estructura se encuentra alterada. Recuerda que  este examen se realiza en mujeres que ya iniciaron su vida sexual. Ante cualquier duda, no olvides consultar con tu ginecólogo Dr. Ego.
                </p>
                <a href="contacto.php" class="solicitar">Solicitar servicio</a>
                <a href="http://www.facebook.com/sharer.php?s=100&amp;p[url]=http://www.drego.pe/consultorio.php&amp;p[title]=DR. EGO | Carlos Calderón Ginecología y Obstetricia&amp;p[summary]=DR. EGO | Carlos Calderón Ginecología y Obstetricia&amp;p[images][0]=http://www.drego.pe/compartir-face/1.jpg" target="_blank" class="faceinfo"><img src="imgs/face.png"></a>

                <div class="clear"></div>
            </div>

            <div class="clear"></div>
        </div>



        <div class="boxconsult">
            <div class="infoconsult">
                <h2 class="titulo"> Dispositivo Intrauterino con Hormonas</h2>
                <p class="texto-gray">Es un dispositivo liberador de hormonas en forma de T que se coloca dentro del útero, es pequeño  e imperceptible, mide de 3 a 5 cm.  Su uso más conocido es como método anticonceptivo, teniendo una gran eficacia  similar con la ligadura de trompas pero reversible (dura 5 años). La ventaja es que sus cantidades hormonales son muy bajas, aminorando los efectos secundarios de las hormonas como el síndrome premenstrual y la ganancia de peso. Otro uso beneficioso es en el tratamiento para el sangrado excesivo. Recuerda que la indicación y la colocación debe ser realizada por tu ginecólogo. Ante cualquier duda, no olvides consultar con tu ginecólogo Dr. Ego.
                </p>
                <a href="contacto.php" class="solicitar">Solicitar servicio</a>
                <a href="http://www.facebook.com/sharer.php?s=100&amp;p[url]=http://www.drego.pe/consultorio.php&amp;p[title]=DR. EGO | Carlos Calderón Ginecología y Obstetricia&amp;p[summary]=DR. EGO | Carlos Calderón Ginecología y Obstetricia&amp;p[images][0]=http://www.drego.pe/compartir-face/1.jpg" target="_blank" class="faceinfo"><img src="imgs/face.png"></a>

                <div class="clear"></div>
            </div>
            <img src="imgs/intra.jpg" class="imgconsulta margright">
            <div class="clear"></div>
        </div>



        <div class="boxconsult">
            <img src="imgs/reju.jpg" class="imgconsulta">
            <div class="infoconsult leftserv">
                <h2 class="titulo"> Rejuvenecimiento Genital Láser</h2>
                <p class="texto-gray">Es el procedimiento por el cual con el uso de láser fraccionado se incide de manera repetitiva sobre las células de colágeno, logrando un tensado vaginal y un rejuvenecimiento del epitelio. También es de gran utilidad en pacientes con incontinencia urinaria leve a moderada sin necesidad de ningún procedimiento quirúrgico. Ante cualquier consulta no dudes en contactar a tu ginecólogo Dr Ego.
                </p>
                <a href="contacto.php" class="solicitar">Solicitar servicio</a>
                <a href="http://www.facebook.com/sharer.php?s=100&amp;p[url]=http://www.drego.pe/consultorio.php&amp;p[title]=DR. EGO | Carlos Calderón Ginecología y Obstetricia&amp;p[summary]=DR. EGO | Carlos Calderón Ginecología y Obstetricia&amp;p[images][0]=http://www.drego.pe/compartir-face/1.jpg" target="_blank" class="faceinfo"><img src="imgs/face.png"></a>

                <div class="clear"></div>
            </div>

            <div class="clear"></div>
        </div>


    </div>



    <div class="clear"></div>
@endsection